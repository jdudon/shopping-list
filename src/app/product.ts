export interface Product {
    id?: number;
    name: string;
    tag: string;
    qty:number;
    comment: string;
    done: boolean;
}
