import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router'; // nécessaire pour utiliser le router d'angular.
import { FormsModule }   from '@angular/forms'; // nécessaire pour avoir accès aux directives ngModel et ngSubmit.
import { HttpClientModule } from '@angular/common/http'; // nécessaire pour faire de l'ajax.

import { AppComponent } from './app.component'; // le component principal de notre application.
import { HomeComponent } from './home/home.component'; // le component qui représente la page d'accueil de notre application.
import { ShoppingListComponent } from './shopping-list/shopping-list.component'; // le component qui représente la page qui affiche une shoppingList en détail.
import { NavbarComponent } from './navbar/navbar.component';
import { ListComponent } from './list/list.component';
import { ProductComponent } from './product/product.component';
import { ProductFormComponent } from './product-form/product-form.component'; // le component qui représente la navbar de l'application.

const routes: Routes = [ // on déclare les routes de l'application dans une constante.
  { path: '', component: HomeComponent }, // une route est caractérisée par deux choses: l'URI de la route et le component qui la représente.
  { path: 'shopping-list/:id', component: ShoppingListComponent } // on peut inserer des paramètres dynamique dans l'URI avec la syntaxe `:val`, comme dans symfony avec {val}.
];

@NgModule({ // le décorateur @NgModule permet de déclarer un module angular
  declarations: [ // la clé déclaration permet de déclarer les components présents dans notre module pour le rendre disponible dans les templates
    AppComponent,
    HomeComponent,
    ShoppingListComponent,
    NavbarComponent,
    ListComponent,
    ProductComponent,
    ProductFormComponent,
  ],
  imports: [ // la clé imports permet d'importer d'autre modules dans le module d'application
    BrowserModule,
    RouterModule.forRoot( // ici, on initialise le router en lui passant les routes déclarées plus haut.
      routes,
      // { enableTracing: true } // <-- debugging purposes only
    ),
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
