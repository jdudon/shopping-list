import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ShoppingList } from './shopping-list';
import { Observable } from 'rxjs';
import { Product } from './product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  
  constructor(private http: HttpClient) { }
  private url:string = "http://localhost:8080/shopping-list/";


  add(product: Product, shoppingListId:number):Observable<Product> {
    return this.http.post<Product>(`${this.url}/${shoppingListId}/product`, product);
  }

}
