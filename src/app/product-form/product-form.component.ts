import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Product } from '../product';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css']
})
export class ProductFormComponent implements OnInit {

  constructor() { }

  @Input() product:Product;
  @Output() productChange = new EventEmitter<Product>();
  @Output() onAdd = new EventEmitter();

  ngOnInit() {
  }

  onSubmit() {
    this.productChange.emit(this.product);
  }

}
