import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ShoppingList } from './shopping-list';

@Injectable({
  providedIn: 'root'
})
export class ShoppingListService {
  // l'url de base de la ressource shoppingList
  private url:string = "http://localhost:8080/shopping-list";
  // on injecte http client pour pouvoir envoyer des requètes en ajax (doit obligatoirement être une private)
  constructor(private http: HttpClient) { }

  // récupère toutes les shoppingLists (dans un tableau)
  all():Observable<ShoppingList[]>{
    return this.http.get<ShoppingList[]>(this.url);
  }

  // supprime une shoppingList
  delete(shoppingList: ShoppingList){
    return this.http.delete(`${this.url}/${shoppingList.id}`);
  }

  // créé une shoppingList, retourne la shoppingList fraichement créée
  add(shoppingList: ShoppingList):Observable<ShoppingList>{
    return this.http.post<ShoppingList>(this.url, shoppingList);
  }
  one(shoppingListId: number):Observable<ShoppingList>{
    return this.http.get<ShoppingList>(`${this.url}/${shoppingListId}`);
  }
}
