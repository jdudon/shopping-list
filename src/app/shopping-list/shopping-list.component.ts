import { Component, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router'; // Activatedroute permet de récupérer la valeur des paramètres d'une route
import { ShoppingListService } from '../shopping-list.service';
import { ProductService } from '../product.service';
import { Product } from '../product';
import { ShoppingList } from '../shopping-list';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent implements OnInit {

    id: number;
    newProduct: Product = {
    name: null,
    tag: null,
    qty: null,
    comment: null,
    done: false
  }

  shoppingList:Partial<ShoppingList> = {};


  constructor(route: ActivatedRoute, private shoppingListService: ShoppingListService, private productService: ProductService) {
    // pour récupérer le paramètre :id de la route, angular nous oblige à manipuler un observable.
    // On peut subscribe à un observable de la même façon qu'on peut utiliser .then sur une promise.
    route.params.subscribe(data => this.id = +data['id']); // le '+' permet de caster (transformer) le type de data['id'] en number
  }

  ngOnInit() {
    this.shoppingListService.one(this.id).subscribe(data => this.shoppingList = data);
  }

  addProduct(){
    this.productService.add(this.newProduct,this.id).subscribe();
  }
  

}
