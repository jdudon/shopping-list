import { Component, OnInit } from '@angular/core';
import { ShoppingList } from '../shopping-list'; // on importe l'interface ShoppingList
import { ShoppingListService } from '../shopping-list.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  // la proprété newShoppingList va recevoir le contenu du formulaire permettant de créer une nouvelle shoppingList
  // pour garantir que les données sont au bon format, on type la propriété avec l'interface ShoppingList
  // pour que cela fonctionne, on doit donner une valeur aux propriétés obligatoires du type ShoppingList (en l'occurence name)
  newShoppingList: ShoppingList = { name: null };

  // la propriété list contient un tableau de shoppingList représentant la liste de toutes les ShoppingList existantes
  shoppingListArray: ShoppingList[];

  // on injecte le ShoppingListService dans le component (doit obligatoirement être une private)
  constructor(private shoppingListService: ShoppingListService) {}

  ngOnInit() {
    this.shoppingListService.all().subscribe(data => this.shoppingListArray = data);
  }

  // la méthode addShoppingList est appelée quand le formulaire de création est submit
  addShoppingList(){
    // on envoie la requête
    this.shoppingListService.add(this.newShoppingList).subscribe(data => {
      // lorsque le serveur répond, on ajoute la nouvelle shoppingList dans le tableau shoppingListArray
      this.shoppingListArray.push(data);
      //  et on réinitialise l'état de la nouvelle shoppingList, ce qui vide le formulaire
      this.newShoppingList = {name: null};
    });
  }
  // la méthode onDelete est appelée quand un des boutons de suppression est cliqué
  onDelete(shoppingList: ShoppingList){
    // on envoie la requête
    this.shoppingListService.delete(shoppingList).subscribe(() => {
      // lorsque le serveur répond, on retire la shoppingList supprimée du tableau shoppingListArray
      this.shoppingListArray = this.shoppingListArray.filter(currentShoppingList => {
        return currentShoppingList.id !== shoppingList.id
      });
    });
  }

}

