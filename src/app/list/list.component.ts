import { Component, OnInit,  Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  // Pour permettre aux components de communiquer,
  // nous devons déclarer des @Input() et des @Output() dans le component enfant.
  // De cette façon, il devient possible d'utiliser les bananas () et les boxes [] dans le template du parent :
  //           +------+
  //           |Parent|
  // (name)="" +-+--+-+ [name]=""
  //           ^      |
  //     event |      | value
  //           |      v
  //   @Output +-+--+-+ @Input
  //           |Enfant|
  //           +------+

  // On déclare l'input `list` qui attend le tableau d'objets à afficher.
  @Input() list: Array<Object>;
  // On déclare l'input `uri` qui permet d'ajouter un lien sur les éléments.
  @Input() uri: string;
  // On déclare l'output `delete`, un output est toujours une instance d'EventEmitter (c'est pour ça qu'on écrit new).
  // Un EventEmitter permet de créer un event angular, un event peut contenir des données.
  // On peut donc définnir quel est le type des données encapsulées dans l'event avec les chevrons : <Type>.
  @Output() delete = new EventEmitter<Object>(); 

  constructor() { }

  ngOnInit() {
  }

  // lorsqu'on clique sur le bouton delete, on déclenche l'event avec `emit` et on passe les données à envoyer en paramètre.
  onClick(item){
    this.delete.emit(item);
  }
 
}

